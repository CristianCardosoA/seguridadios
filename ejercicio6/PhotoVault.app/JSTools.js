function MyAppGetHTMLElementsAtPoint(x,y) {
    var tags = [];
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.tagName) {
            tags.push(e.tagName);
        }
        e = e.parentNode;
    }
    return tags.join(',');
}

function MyAppGetLinkSRCAtPoint(x,y) {
    var tags = "";
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.src) {
            tags += e.src;
            break;
        }
        e = e.parentNode;
    }
    return tags;
}

function MyAppGetLinkHREFAtPoint(x,y) {
    var tags = "";
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.href) {
            tags += e.href;
            break;
        }
        e = e.parentNode;
    }
    return tags;
}

function documentCoordinateToViewportCoordinate(x,y) {
    var coord = new Object();
    coord.x = x - window.pageXOffset;
    coord.y = y - window.pageYOffset;
    return coord;
}

function viewportCoordinateToDocumentCoordinate(x,y) {
    var coord = new Object();
    coord.x = x + window.pageXOffset;
    coord.y = y + window.pageYOffset;
    return coord;
}

function elementFromPointIsUsingViewPortCoordinates() {
    if (window.pageYOffset > 0) {     // page scrolled down
        return (window.document.elementFromPoint(0, window.pageYOffset + window.innerHeight -1) == null);
    } else if (window.pageXOffset > 0) {   // page scrolled to the right
        return (window.document.elementFromPoint(window.pageXOffset + window.innerWidth -1, 0) == null);
    }
    return false; // no scrolling, don't care
}


// coordinates fix: http://www.icab.de/blog/2011/10/17/elementfrompoint-under-ios-5/
function elementFromDocumentPoint(x,y) {
    if (elementFromPointIsUsingViewPortCoordinates()) {
        var coord = documentCoordinateToViewportCoordinate(x,y);
        return window.document.elementFromPoint(coord.x,coord.y);
    } else {
        return window.document.elementFromPoint(x,y);
    }
}

function elementFromViewportPoint(x,y) {
    if (elementFromPointIsUsingViewPortCoordinates()) {
        return window.document.elementFromPoint(x,y);
    } else {
        var coord = viewportCoordinateToDocumentCoordinate(x,y);
        return window.document.elementFromPoint(coord.x,coord.y);
    }
}

