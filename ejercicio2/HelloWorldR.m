#import <Foundation/Foundation.h>


@interface SaySomething : NSObject
- (void) say: (NSString *) phrase;
@end

@implementation SaySomething

- (void) say: (NSString *) phrase {
        printf("%s\n", [ phrase UTF8String ]);
}
@end

int main(void) {
	@autoreleasepool {
		Class myclass = NSClassFromString(@"SaySomething");
		id saySomething = [ [ myclass alloc ] init ];
		SEL selector =  NSSelectorFromString(@"say:");
		[ saySomething performSelector: selector withObject: @"Hello, world!" ];
		return 0;
	}
}
