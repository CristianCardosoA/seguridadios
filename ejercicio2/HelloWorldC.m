#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <objc/message.h>

@interface SaySomething : NSObject
- (void) say: (NSString *) phrase;
@end

@implementation SaySomething

- (void) say: (NSString *) phrase {
        printf("%s\n", [ phrase UTF8String ]);
}
@end

int main(void) {

	objc_msgSend(
		objc_msgSend( objc_getClass("NSAutoReleasePool"), NSSelectorFromString(@"alloc")),
		NSSelectorFromString(@"init")
	);


	return 0;
}
